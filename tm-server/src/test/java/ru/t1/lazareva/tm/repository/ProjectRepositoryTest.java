package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.comparator.NameComparator;
import ru.t1.lazareva.tm.marker.UnitCategory;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.service.ConnectionService;
import ru.t1.lazareva.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

import static ru.t1.lazareva.tm.constant.ProjectTestData.*;
import static ru.t1.lazareva.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.lazareva.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connection);

    @NotNull
    private final IProjectRepository emptyRepository = new ProjectRepository(connection);

    @Before
    public void before() throws Exception {
        repository.add(USER_PROJECT1);
        repository.add(USER_PROJECT2);
    }

    @After
    public void after() throws Exception {
        repository.removeAll(PROJECT_LIST);
    }

    @Test(expected = NullPointerException.class)
    public void add() throws Exception {
        Assert.assertNull(repository.add(NULL_PROJECT));
        Assert.assertNotNull(repository.add(ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void addMany() throws Exception {
        Assert.assertNotNull(repository.add(ADMIN_PROJECT_LIST));
        for (final Project project : ADMIN_PROJECT_LIST)
            Assert.assertEquals(project, repository.findOneById(project.getId()));
    }

    @Test(expected = NullPointerException.class)
    public void addByUserId() throws Exception {
        Assert.assertNull(repository.add(ADMIN_TEST.getId(), NULL_PROJECT));
        Assert.assertNull(repository.add(null, ADMIN_PROJECT1));
        Assert.assertNotNull(repository.add(ADMIN_TEST.getId(), ADMIN_PROJECT1));
        @Nullable final Project project = repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1, project);
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final Project project = repository.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName());
        Assert.assertEquals(project, repository.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final Project project = repository.create(ADMIN_TEST.getId(), ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(project, repository.findOneById(ADMIN_TEST.getId(), project.getId()));
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(ADMIN_TEST.getId(), project.getUserId());
    }

    @Test
    public void set() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.set(ADMIN_PROJECT_LIST);
        Assert.assertEquals(ADMIN_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAll() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        Assert.assertEquals(USER_PROJECT_LIST, emptyRepository.findAll());
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        Assert.assertEquals(USER_PROJECT_LIST, repository.findAll(USER_TEST.getId()));
    }

    @Test
    public void findAllComparator() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.add(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(SORTED_PROJECT_LIST, emptyRepository.findAll(comparator));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        Assert.assertEquals(USER_PROJECT_LIST.stream().sorted(comparator).collect(Collectors.toList()), repository.findAll(USER_TEST.getId(), comparator));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(repository.existsById(NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertFalse(repository.existsById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId(), USER_PROJECT1.getId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertNull(repository.findOneById(null));
        Assert.assertNull(repository.findOneById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), null));
        Assert.assertNull(repository.findOneById(USER_TEST.getId(), NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = repository.findOneById(USER_TEST.getId(), USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test(expected = NullPointerException.class)
    public void findOneByIndexByUserId() throws Exception {
        Assert.assertNull(repository.findOneByIndex(USER_TEST.getId(), null));
        final int index = repository.findAll().indexOf(USER_PROJECT1);
        @Nullable final Project project = repository.findOneByIndex(USER_TEST.getId(), index);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1, project);
    }

    @Test
    public void clear() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT_LIST);
        emptyRepository.clear();
        Assert.assertEquals(0, emptyRepository.getSize());
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(USER_PROJECT1);
        emptyRepository.add(USER_PROJECT2);
        emptyRepository.clear(USER_TEST.getId());
        Assert.assertEquals(0, emptyRepository.getSize(USER_TEST.getId()));
    }

    @Test
    public void remove() throws Exception {
        Assert.assertNull(repository.remove(null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = repository.remove(createdProject);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        Assert.assertNull(repository.remove(ADMIN_TEST.getId(), null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        Assert.assertNull(repository.remove(null, createdProject));
        @Nullable final Project removedProject = repository.remove(ADMIN_TEST.getId(), createdProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    public void removeById() throws Exception {
        Assert.assertNull(repository.removeById(null));
        Assert.assertNull(repository.removeById(NON_EXISTING_PROJECT_ID));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        @Nullable final Project removedProject = repository.removeById(ADMIN_PROJECT1.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), null));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), NON_EXISTING_PROJECT_ID));
        Assert.assertNull(repository.removeById(ADMIN_TEST.getId(), USER_PROJECT1.getId()));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        Assert.assertNull(repository.removeById(null, createdProject.getId()));
        @Nullable final Project removedProject = repository.removeById(ADMIN_TEST.getId(), createdProject.getId());
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    public void removeByIndex() throws Exception {
        Assert.assertNull(repository.removeByIndex(null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll().indexOf(createdProject);
        @Nullable final Project removedProject = repository.removeByIndex(index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeByIndexByUserId() throws Exception {
        Assert.assertNull(repository.removeByIndex(ADMIN_TEST.getId(), null));
        @Nullable final Project createdProject = repository.add(ADMIN_PROJECT1);
        final int index = repository.findAll(ADMIN_TEST.getId()).indexOf(createdProject);
        Assert.assertNull(repository.removeByIndex(null, index));
        @Nullable final Project removedProject = repository.removeByIndex(ADMIN_TEST.getId(), index);
        Assert.assertNotNull(removedProject);
        Assert.assertEquals(ADMIN_PROJECT1, removedProject);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId(), ADMIN_PROJECT1.getId()));
    }

    @Test
    public void getSize() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize());
        emptyRepository.add(ADMIN_PROJECT1);
        Assert.assertEquals(1, emptyRepository.getSize());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        Assert.assertEquals(0, emptyRepository.getSize(ADMIN_TEST.getId()));
        emptyRepository.add(ADMIN_PROJECT1);
        emptyRepository.add(USER_PROJECT1);
        Assert.assertEquals(1, emptyRepository.getSize(ADMIN_TEST.getId()));
    }

    @Test
    public void removeAll() throws Exception {
        Assert.assertTrue(emptyRepository.findAll().isEmpty());
        emptyRepository.add(PROJECT_LIST);
        emptyRepository.removeAll(PROJECT_LIST);
        Assert.assertEquals(0, emptyRepository.getSize());
    }

}
