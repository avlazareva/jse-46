package ru.t1.lazareva.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;


    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEMFactory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEMFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDBDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDBUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDBUser());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDBPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDBDialect());
        settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, "true");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDBShowSQL());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        settings.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBCache());
        settings.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheRegion());
        settings.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, propertyService.getDBQueryCache());
        settings.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, propertyService.getDBMinimalPuts());
        settings.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
        settings.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheProvider());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDto.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDto.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDto.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDto.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
